<?php

echo '<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Na pewno chcesz się wylogować?</h5>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-gray" type="button" data-dismiss="modal">Anuluj</button>
                        <a class="btn btn-black" href="admin/wylogowanie" target="_self">Wyloguj</a>
                    </div>
                </div>
            </div>
        </div>';

?>