'use strict';

var app = angular.module('app', ['ngRoute', 'controllersAdmin']);


app.config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider ){


    //=====================ADMIN=====================//

    //=====================Strona główna=====================//

    $routeProvider.when('/admin', {

        controller: 'homePageAdmin',
        templateUrl: 'application/partials/admin/home.html',
        resolve: {
            resolvedVal: function() {

                ///=== active links ===///

                var activeLinks = $('.sidebar .nav-item');

                var numItems = $('.sidebar .nav-item').length - 1;             

                for (var i = 0; i <= numItems; i++) {
                    $(activeLinks[i]).removeClass( "active");
                }

                $('.dashboard').addClass('active');

            }
        }

    });

    $routeProvider.otherwise({
        redirectTo: '/'
    });

    $locationProvider.html5Mode(true);

}]);




