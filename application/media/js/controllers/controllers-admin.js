'use strict';

var controllersAdmin = angular.module('controllersAdmin', ['angularFileUpload', 'myDirectives'] );

controllersAdmin.controller( 'homePageAdmin', ['$scope', '$http', function($scope, $http ){

    $http({
        method: "POST",
        url: "",
        headers: {
            'Naglowek': 'mój naglowek',
        }
    }).
    success( function(data){

        $scope = data;

    }).error( function(){

        console.log('Błąd pobrania pliku json');

    });

}]);