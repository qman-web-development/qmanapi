'use strict';

var controllersSite = angular.module('controllersSite', [] );

controllersSite.controller( 'homePage', ['$scope', '$http', function($scope, $http ){

    $http({
        method: "POST",
        url: "",
        headers: {
            'Naglowek': 'mój naglowek',
        }
    }).
    success( function(data){

        $scope = data;

    }).error( function(){

        console.log('Błąd pobrania pliku json');

    });

}]);