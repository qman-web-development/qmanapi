'use strict';

var app = angular.module('app', ['ngRoute', 'controllersSite']);


app.config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider ){

    //=====================SITE=====================//
    
    //=====================Strona główna=====================//
    
    $routeProvider.when('/', {

        controller: 'homePage',
        templateUrl: 'application/partials/site/home.html'

    });
	
    $routeProvider.otherwise({
        redirectTo: '/'
        

    });
      
    $locationProvider.html5Mode(true);
	
}]);




