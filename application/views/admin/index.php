<!DOCTYPE html>
<html lang="pl">

    <head>

        <?=add_metatags()?>

        <?=add_title("qman")?>

        <?=add_basehref()?>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <?=stylesheet_load('bootstrap.min.css, admin/sb-admin.css')?>

        <?=icon_load("favicon.png")?> 

    </head>

    <body id="page-top" ng-app="app" class="sidebar-toggled">

        <?=module_load('ADMINNAVIGATION')?>

        <div id="wrapper">

            <!--/\\\/\\\/\\\/\\\/\\\/\\\-Sidebar-/\\\/\\\/\\\/\\\/\\\/\\\-->
            <?=module_load('ADMINSIDEBAR')?>

            <div ng-view></div>

            <!--/\\\/\\\/\\\/\\\/\\\/\\\-Sticky Footer-/\\\/\\\/\\\/\\\/\\\/\\\-->
            <?=module_load('ADMINFOOTER')?>

        </div>

        <!--/\\\/\\\/\\\/\\\/\\\/\\\-Scroll to Top Button-/\\\/\\\/\\\/\\\/\\\/\\\-->
        <?=module_load('SCROLLTOTOP')?>
        
        <!--/\\\/\\\/\\\/\\\/\\\/\\\-Logout Modal-/\\\/\\\/\\\/\\\/\\\/\\\-->
        <?=module_load('LOGOUT')?>
        
        <?=javascript_load('angular.min.js, angular-route.min.js, angular-file-upload.min.js, moment-with-locales.min.js, application-admin.js, directives.js, controllers/controllers-admin.js, jquery.min.js, popper.min.js, bootstrap.min.js, jquery.easing.min.js, admin/sb-admin.js')?>

    </body>

</html>
