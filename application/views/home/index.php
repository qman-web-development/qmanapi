<!DOCTYPE html>
<html lang="pl">
    <head>

        <?=add_metatags()?>

        <?=add_title("qman")?>
        
        <?=add_basehref()?>
        
        <?=stylesheet_load('bootstrap.min.css?no_compress, main.css')?>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

        <link rel="stylesheet" href="<?=directory_stylesheet()?>site/style.css"> 
        
        <?=icon_load("favicon.png")?> 

    </head> 
    <body ng-app="app">

        <div ng-view></div>

        <?=module_load('FOOTER')?>
        
        <?=javascript_load('angular.min.js, angular-route.min.js, angular-file-upload.min.js, application.js, directives.js, controllers/controllers-site.js, jquery.min.js, bootstrap.min.js, site/script.js')?>

    </body>
</html>