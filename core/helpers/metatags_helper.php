<?php

define("METATAGS", true);

function add_metatags()
{
    echo "\n";
    echo '<meta charset="utf-8">';
    echo "\n";
    echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
    echo "\n";
    echo '<meta name="description" content="">';
    echo "\n";
    echo '<meta name="keywords" content="">';
}

?>