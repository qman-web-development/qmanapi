<?php

define("HEAD", true);

function icon_load($url)
{
    if(!empty($url))
    {
        $url = (substr($url, 0, 1) == "/") ? subst($url, 1) : $url;
        return "<link href=\"".SERVER_ADDRESS."application/media/images/".$url."\" rel=\"shortcut icon\" type=\"image/png\" />\n";
    }
    else 
    {
        return ;
    }
}

function add_title($title) 
{
    if(!empty($title))
    {
        return "<title>{$title}</title>";
    }
    else
    {
        return ;
    }
}

function add_basehref() 
{

        return "<base href=\"".SERVER_ADDRESS."\" />";

}

?>