<?php

define("DIRECTORY", true);

function directory_images()
{
    $config = registry::register("config");
    return $config->app_images_path;
}

function directory_video()
{
    $config = registry::register("config");
    return $config->app_video_path;
}

function directory_javascript()
{
    $config = registry::register("config");
    return $config->app_javascript_path;
}

function directory_stylesheet()
{
    $config = registry::register("config");
    return $config->app_stylesheet_path;
}

function directory_fonts()
{
    $config = registry::register("config");
    return $config->app_fonts_path;
}

?>