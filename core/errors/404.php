<!DOCTYPE html>
<html lang="pl">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nie znaleziono nic pod tym adresem...">
        <meta name="keywords" content="błąd 404">

        <title>Nie znaleziono strony...</title>

        <link rel="stylesheet" href="../../application/media/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../application/media/css/main.css">
        
        <link rel="shortcut icon" type="image/png" href="../../application/media/images/favicon.png">

    </head>
    <body class="bg-404">

        <div class="container all-height">

            <div class="row justify-content-center align-items-center all-height">
                <div>
                    <img class="img-fluid" src="../../application/media/images/error_404.png">
                    <p class="text-center white-font">Nie znaleziono nic pod tym adresem...</p>
                    <p class="text-center"><a href="../../">Wróć do strony głównej</a></p>
                </div>
            </div>

        </div>

        <script src="../../application/media/js/jquery.min.js"></script>
        <script src="../../application/media/js/bootstrap.min.js"></script>

    </body>
</html>